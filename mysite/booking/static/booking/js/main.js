/*  ---------------------------------------------------
    Template Name: Sona
    Description: Sona Hotel Html Template
    Author: Colorlib
    Author URI: https://colorlib.com
    Version: 1.0
    Created: Colorlib
---------------------------------------------------------  */

'use strict';

(function ($) {

    /*------------------
        Preloader
    --------------------*/
    $(window).on('load', function () {
        $(".loader").fadeOut();
        $("#preloder").delay(200).fadeOut("slow");
    });

    /*------------------
        Background Set
    --------------------*/
    $('.set-bg').each(function () {
        var bg = $(this).data('setbg');
        $(this).css('background-image', 'url(' + bg + ')');
    });

    //Offcanvas Menu
    $(".canvas-open").on('click', function () {
        $(".offcanvas-menu-wrapper").addClass("show-offcanvas-menu-wrapper");
        $(".offcanvas-menu-overlay").addClass("active");
    });

    $(".canvas-close, .offcanvas-menu-overlay").on('click', function () {
        $(".offcanvas-menu-wrapper").removeClass("show-offcanvas-menu-wrapper");
        $(".offcanvas-menu-overlay").removeClass("active");
    });

    // Search model
    $('.search-switch').on('click', function () {
        $('.search-model').fadeIn(400);
    });

    $('.search-close-switch').on('click', function () {
        $('.search-model').fadeOut(400, function () {
            $('#search-input').val('');
        });
    });

    /*------------------
		Navigation
	--------------------*/
    $(".mobile-menu").slicknav({
        prependTo: '#mobile-menu-wrap',
        allowParentLinks: true
    });

    /*------------------
        Hero Slider
    --------------------*/
   $(".hero-slider").owlCarousel({
        loop: true,
        margin: 0,
        items: 1,
        dots: true,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        smartSpeed: 1200,
        autoHeight: false,
        autoplay: true,
        mouseDrag: false
    });

    /*------------------------
		Testimonial Slider
    ----------------------- */
    $(".testimonial-slider").owlCarousel({
        items: 1,
        dots: false,
        autoplay: true,
        loop: true,
        smartSpeed: 1200,
        nav: true,
        navText: ["<i class='arrow_left'></i>", "<i class='arrow_right'></i>"]
    });

    /*------------------
        Magnific Popup
    --------------------*/
    $('.video-popup').magnificPopup({
        type: 'iframe'
    });

    /*------------------
		Nice Select
	--------------------*/
    $("select").niceSelect();

})(jQuery);

// Убавляем кол-во по клику
    $('.quantity_inner .bt_minus').click(function() {
    let $input = $(this).parent().find('.quantity');
    let $input2 = $(this).parent().find('.pricess');
    let count = parseInt($input.val()) - 1;
    let price = parseInt($input2.val()) - 200;
    $('.final_price').val(count > -1 ? parseFloat($('.final_price').val()) - 200 : parseFloat($('.final_price').val()))
    count = count < 0 ? 0 : count;
    price = price < 0 ? 0 : price
    $input.val(count);
    $input2.val(price);
});
// Прибавляем кол-во по клику
$('.price_change .quantity_inner .bt_plus').click(function() {
    let $input = $(this).parent().find('.quantity');
    let $input2 = $(this).parent().find('.pricess');
    let count = parseInt($input.val()) + 1;
    let price = parseInt($input2.val()) + 200;
    $('.final_price').val(count < 5 ? parseFloat($('.final_price').val()) + 200 : parseFloat($('.final_price').val()))
    count = count > parseInt($input.data('max-count')) ? parseInt($input.data('max-count')) : count;
    price = price > parseInt($input2.data('max-count')) ? parseInt($input2.data('max-count')) : price;
    $input.val(parseInt(count));
    $input2.val(parseInt(price));
});
// Убираем все лишнее и невозможное при изменении поля
$('.quantity_inner .quantity').bind("change keyup input click", function() {
    if (this.value.match(/[^0-9]/g)) {
        this.value = this.value.replace(/[^0-9]/g, '');
    }
    if (this.value == "") {
        this.value = 0;
    }
    if (this.value > parseInt($(this).data('max-count'))) {
        this.value = parseInt($(this).data('max-count'));
    }
});

//Чек бокс для корзины

$('.checkbox-ios input').on('change', function(e){
	const {checked} = e.target
	$('.price_bag').val(checked ? 200 : 0)
	$('.final_price').val(checked ? parseFloat($('.final_price').val()) + 200 : parseFloat($('.final_price').val()) - 200)
});

//$('.checkbox-ios input').focusout(function(){
//	$(this).parent().removeClass('focused');
//});