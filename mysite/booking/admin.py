from django.contrib import admin
from .models import *

admin.site.register(Times)
admin.site.register(Bookings)
admin.site.register(Coach)
admin.site.register(CoachesTimes)