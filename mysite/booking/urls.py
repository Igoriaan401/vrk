from django.urls import path

from .views import *

urlpatterns = [
    path('', index, name='home'),
    path('about/', about, name='about'),
    path('contact/', contact, name='contact'),
    path('booking_place/<slug:date>/', booking, name='booking'),
    path('reservation/<slug:time_id>/', final_reservation, name='reservation'),
    path('register/', register, name='register'),
    path('login/', user_login, name='login'),
    path('logout/', logoutuser, name='logout'),
    path('profile/<slug:username>/', profile, name='profile'),
    path('delete/<int:id>/', confirm_delete, name='post-delete')
]