from django.contrib.auth import logout, get_user_model, authenticate, login
from django.core.mail import send_mail, BadHeaderError
from django.shortcuts import render, get_object_or_404, redirect
from django.shortcuts import HttpResponse, HttpResponseRedirect
from .models import *
from django.contrib import messages
from .forms import *
from django.template.defaulttags import register
import datetime

now = datetime.date.today()


@register.filter
def get_range(value):
    return range(value)


@register.filter()
def get_minus_range(value):
    return range(5-value)


def index(request):
    reviews = Reviews.objects.order_by("-id")[0:3]
    context = {
        'reviews': reviews,
        'time': now,
    }

    return render(request, 'booking/index.html', context=context)


def about(request):
    coach = Coach.objects.order_by("id")
    print(coach)
    context = {
        'coach': coach,
        'time': now,
    }
    return render(request, 'booking/about-us.html', context=context)


# def contact(request):
#     return render(request, 'booking/contact.html')


def booking(request, date):
    if request.method == 'POST':
        form = ChangeDate(request.POST)
        if form.is_valid():
            date_change = form.cleaned_data['change_date']
            return HttpResponseRedirect('/booking/booking_place/' + str(date_change) + '/')

    date_selected = Days.objects.filter(booking_day=date).first()
    if date_selected is None:
        times_1 = []
        times_2 = []
        times_3 = []
        times_4 = []

        form = ChangeDate()

        context = {
            'times1': times_1,
            'times2': times_2,
            'times3': times_3,
            'times4': times_4,
            'form': form,
            'time': now,
        }
        return render(request, 'booking/booking.html', context=context)
    else:
        times_1 = Times.objects.filter(days_id=date_selected.id, court_id=1).order_by('pk')
        times_2 = Times.objects.filter(days_id=date_selected.id, court_id=2).order_by('pk')
        times_3 = Times.objects.filter(days_id=date_selected.id, court_id=3).order_by('pk')
        times_4 = Times.objects.filter(days_id=date_selected.id, court_id=4).order_by('pk')

        form = ChangeDate()

        context = {
            'times1': times_1,
            'times2': times_2,
            'times3': times_3,
            'times4': times_4,
            'form': form,
            'time': now,
        }
        return render(request, 'booking/booking.html', context=context)


def final_reservation(request, time_id):
    times = get_object_or_404(Times, pk=time_id)
    coach = CoachesTimes.objects.filter(days_id=times.days_id, period_time=times.period_time)
    curent_user = request.user.id

    if request.method == 'POST':
        final_price = request.POST['final_price']

        selected_coach = request.POST['coach']

        times.is_free = False
        times.save(update_fields=["is_free"])

        bookings = Bookings()
        bookings.days = times.days.booking_day
        bookings.booking_time = times.period_time
        bookings.user_id = curent_user
        bookings.booking_price = final_price
        bookings.is_paid = False
        bookings.time = Times(id=time_id)
        if selected_coach == "0":
            bookings.selected_coach = None
        else:
            st_coach = get_object_or_404(CoachesTimes, pk=selected_coach)
            st_coach.is_free = False
            st_coach.save(update_fields=["is_free"])
            bookings.selected_coach = st_coach.pk

        bookings.save()

        return redirect('home')

    context = {
        'times': times,
        'time': now,
        'coach': coach,
    }

    return render(request, 'booking/final_reservation.html', context=context)


def register(request):
    if request.method == 'POST':
        form = RegisterUserForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f'Создан аккаунт {username}!')
            return redirect('home')
    else:
        form = RegisterUserForm()
    return render(request, 'booking/register.html', {'form': form, 'time': now})

#igorqwe 162534OvOsH


def user_login(request):
    if not request.user.is_authenticated:
        if request.method == 'POST':
            fm = LoginUserForm(request=request, data=request.POST)
            if fm.is_valid():
                uname = fm.cleaned_data['username']
                upass = fm.cleaned_data['password']
                user = authenticate(username=uname, password=upass)
                if user is not None:
                    login(request, user)
                    messages.success(request, 'Logged in successfully!!!')
                    return redirect('home')
        else:
            fm = LoginUserForm()
        return render(request, 'booking/login.html', {'form': fm, 'time': now})
    else:
        return redirect('home')


def logoutuser(request):
    logout(request)
    return redirect('home')


def profile(request, username):
    if request.method == 'POST':
        pass

    curent_user = request.user.id

    user = get_user_model().objects.filter(id=curent_user).first()
    user_booking = Bookings.objects.filter(user_id=curent_user).all().order_by('pk')
    if user:
        form = UserUpdateForm(instance=user)
        return render(request, 'booking/profile.html', context={'form': form, 'user_booking': user_booking, 'time': now})

    return redirect('home')


def confirm_delete(request, id):
    book = get_object_or_404(Bookings, pk=id)
    times = get_object_or_404(Times, pk=book.time_id)
    context = {
        'book': book,
        'time': now,
    }

    if request.method == 'GET':
        return render(request, 'booking/confirm_delete.html', context=context)
    else:
        book.delete()
        times.is_free = True
        times.save(update_fields=["is_free"])
        if book.selected_coach is not None:
            ch_times = get_object_or_404(CoachesTimes, pk=book.selected_coach)
            ch_times.is_free = True
            ch_times.save(update_fields=["is_free"])
        messages.success(request, 'Бронирование отменено')
        return redirect('home')


def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)
        if form.is_valid():
            name = form.cleaned_data['name']
            user_email = form.cleaned_data['user_email']
            message = form.cleaned_data['message']
            recipients = ['levai.igor@mail.ru']
            result_message = f'{message}\nПочта для связи со мной:\n{user_email}\nС уважением, {name}'
            try:
                send_mail(user_email, result_message, 'musalimov.ighor@mail.ru', recipients)
                return HttpResponseRedirect('/booking/')
            except BadHeaderError:
                return HttpResponse('invalid header found')
        else:
            form = ContactForm()
    else:
        form = ContactForm()
    context = {
        'form': form,
        'time': now,
    }
    return render(request, 'booking/contact.html', context)


