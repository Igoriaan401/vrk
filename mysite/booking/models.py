from django.db import models
from datetime import datetime, date

from django.shortcuts import redirect
from django.urls import reverse


class Days(models.Model):
    booking_day = models.DateField(auto_now=False, auto_now_add=False)


class Courts(models.Model):
    court_number = models.IntegerField()

    def __str__(self):
        return self.court_number


class Times(models.Model):
    period_time = models.CharField(max_length=255)
    price = models.DecimalField(max_length=255, max_digits=6, decimal_places=2)
    is_free = models.BooleanField(default=True)
    court = models.ForeignKey('Courts', on_delete=models.PROTECT, null=True)
    days = models.ForeignKey('Days', on_delete=models.PROTECT, null=True)

    def __str__(self):
        return self.period_time

    def get_absolute_url(self):
        return reverse('reservation', kwargs={'time_id': self.pk})

    class Meta:
        verbose_name = 'Расписание'
        verbose_name_plural = 'Расписание'
        ordering = ['id']


class ConcactMessage(models.Model):
    name = models.CharField(max_length=255)
    user_email = models.CharField(max_length=255)
    message = models.CharField(max_length=255)

    def __str__(self):
        return self.user_email


class Bookings(models.Model):
    days = models.DateField(auto_now=False, auto_now_add=False, null=True, default=None)
    booking_time = models.CharField(max_length=255)
    user_id = models.IntegerField()
    booking_price = models.DecimalField(max_length=255, max_digits=6, decimal_places=2)
    is_paid = models.BooleanField(default=False)
    selected_coach = models.CharField(max_length=255, null=True)
    time = models.ForeignKey('Times', on_delete=models.PROTECT, null=True)

    class Meta:
        verbose_name = 'Бронирования'
        verbose_name_plural = 'Бронирования'
        ordering = ['id']


class Reviews(models.Model):
    reviewers_name = models.CharField(max_length=255)
    stars_count = models.IntegerField()
    comment = models.CharField(max_length=500)


class Coach(models.Model):
    name = models.CharField(max_length=255)
    surname = models.CharField(max_length=255)
    achievements = models.CharField(max_length=255)
    details = models.CharField(max_length=255)
    price = models.CharField(max_length=255)
    photo = models.ImageField(upload_to="photo/%Y/%m/%d/", null=True)

    class Meta:
        verbose_name = 'Тренеры'
        verbose_name_plural = 'Тренеры'
        ordering = ['id']


class CoachesTimes(models.Model):
    period_time = models.CharField(max_length=255)
    is_free = models.BooleanField(default=True)
    price = models.CharField(max_length=255)
    days = models.ForeignKey('Days', on_delete=models.PROTECT, null=True)
    coaches = models.ForeignKey('Coach', on_delete=models.PROTECT, null=True)

    class Meta:
        verbose_name = 'Расписание  тренеров'
        verbose_name_plural = 'Расписание тренеров'
        ordering = ['id']
